<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="wrapper">
    <div id="blur">
        <header class="js-header">
            <div class="container -header">
                <div class="bar-wrap">
                    <div class="bar-chart primary" data-total="" animated>
                        <span class="bar-chart--inner"></span>
                    </div>
                </div>
                <nav class="navbar navbar-light bg-faded">
                    <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#navbar-header" aria-controls="navbar-header">
                        ☰
                    </button>
                    <a class="nav-logo" href="#">
                        LAVNIK
                        <br>
                        <span class="nav-logo__middle">— &amp; —</span>
                        <br>
                        <span class="nav-logo__bottom">PARTNERS</span>
                    </a>
                    <a class="tel-mob" href="tel:+74953082290"><img src="/_img/tel.png" alt=""></a>
                    <div class="collapse navbar-toggleable-xs" id="navbar-header">
                        <a class="navbar-brand" href="#top">LAVNIK
                            <br>
                            <span class="navbar-brand__middle">— &amp; —</span>
                            <br>
                            <span class="navbar-brand__bottom">PARTNERS</span>
                        </a>
                        <ul class="nav navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link js-goto" data-id="#advantages" href="">Преимущества
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link js-goto" data-id="#outsourcing" href="">Аутсорсинг</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-goto" data-id="#contacts" href="">Контакты</a>
                            </li>
                        </ul>
                        <div class="pull-xs-right">
                            <a class="tel" href="tel:+74953082290">+7 (495) 308-22-90</a>
                            <a class="btn btn-outline-success -tel form1">Заказать звонок</a>
                        </div>
                    </div>
                </nav>
                <!-- /navbar -->
            </div>
        </header>
