<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<footer>
    <div class="container">
        <div class="wrapper flex-justify-between">
            <div class="inner">
                <nav class="nav flex-justify-left flex-wrap">

                    <a class="nav__link" href="#top">О компании</a>
                    <a class="nav__link" href="#key">Бухгалтерия под ключ</a>
                    <a class="nav__link" href="#accountant">Подбор бухгалтера</a>
                    <a class="nav__link" href="#outsourcing">Аутсорсинг</a>
                    <a class="nav__link" href="#contacts">Контакты</a>
                    <a download="" class="nav__link" href="<?= getSettingsProp("FILE") ?>">Политика
                        конфиденциальности</a>

                </nav>

                <div class="socials flex-justify-between">
                    <a href="<?= getSettingsProp("LINK_INSTA") ?>"> <img alt="insta" src="/_img/insta.png"></a>
                    <a href="viber://chat?number=+79647294959"><img src="/_img/viber.png" alt="viber"></a>
                    <a href="whatsapp://send?phone=79647294959"><img src="/_img/whatsapp.png" alt="whatsapp"></a>
                    <a href="tg://resolve?domain=LavnikPartners"><img src="/_img/telegram.png" alt="telegram"></a>
                    <a href="skype:live:9b511276adb1437e?chat"><img src="/_img/skype.png" alt="skype"></a>
                    <div class="decoration"></div>
                </div>
            </div>
            <div class="inner -left">
                <div>ИНН: 773383803</div>
                <div>ОГРН: 11377462988106</div>
                <div>&copy; 2008 &mdash; 2018. Компания &laquo;Лавник и партнеры&raquo;
                    <br> Качественные бухгалтерские услуги в Москве.
                </div>
                <div>Сделали в
                    <a href="https://ms-net.ru/">&laquo;Медиасеть&raquo;</a>
                </div>

            </div>
        </div>
    </div>
</footer>

<div id="popup1" class="overlay">
    <a class="modal_close">
        <div>+</div>
    </a>
    <div class="popup">
        <h2 class="caption -gradient">Заказать звонок</h2>
        <p class="content -paragraph">
            Оставьте, пожалуйста, ваши контактные данные
            <br> и наш менеджер свяжется с вами в удобное для вас время.
        </p>
        <form class="form popup__form flex-space-between js-form-submit" action="">
            <input type="hidden" value="FORM" name="EVENT"/>
            <input type="hidden" value="REQUEST" name="GOAL"/>
            <input class="ms-form__input" type="hidden" value="Заказать звонок" name="THEME"/>

            <input class="area req" type="text" name="NAME" placeholder="Ваше имя">
            <br>
            <input class="area req" type="text" name="PHONE" id="phone" placeholder="Ваш телефон">
            <br>
            <input class="submit" type="submit" value="Заказать">
        </form>
    </div>
</div>
<div id="popup2" class="overlay">
    <a class="modal_close">
        <div>+</div>
    </a>
    <div class="popup">
        <h2 class="caption -gradient">Заказать услугу</h2>
        <p class="content -paragraph">
            Оставьте, пожалуйста, ваши контактные данные
            <br> и наш менеджер свяжется с вами в удобное для вас время.
        </p>
        <form class="form popup__form flex-space-between js-form-submit" action="">
            <input type="hidden" value="USLUGA" name="EVENT"/>
            <input type="hidden" value="REQUEST" name="GOAL"/>
            <input type="hidden" value="USLUGA" name="SEND_FORM"/>
            <div class="caption">Услуга</div>
            <?
            $systype = array(
                1 => "Выстроим систему бухгалтерского учета",
                2 => "Найдем квалифицированного бухгалтера за 1 месяц",
            );

            ?>
            <select id="select" class="area jq-selectbox" name="SYSTEMA">
                <? foreach ($systype as $sys) { ?>
                    <option value="<?= $sys ?>">
                        <?= $sys; ?>
                    </option>
                <? } ?>
            </select>

            <input class="area req" type="text" name="NAME" placeholder="Ваше имя">
            <br>
            <input class="area req" type="text" name="PHONE" id="phone" placeholder="Ваш телефон">
            <br>
            <input class="submit" type="submit" value="Заказать">
        </form>
    </div>
</div>
<div id="popup3" class="overlay">
    <a class="modal_close">
        <div>+</div>
    </a>
    <div class="popup">
        <h2 class="caption -gradient">Заказать услугу</h2>
        <p class="content -paragraph">
            Оставьте, пожалуйста, ваши контактные данные
            <br> и наш менеджер свяжется с вами в удобное для вас время.
        </p>
        <form class="form popup__form flex-space-between js-form-submit" action="">
            <input type="hidden" value="USLUGA" name="EVENT"/>
            <input type="hidden" value="REQUEST" name="GOAL"/>
            <div class="caption">Услуга</div>
            <select id="s_1" class="area jq-selectbox" name="SYSTEMA">
                <option name="SYSTEMA">Возьмем бухгалтерию на аутсорсинг</option>
            </select>
            <?
            $orgtype = array(
                1 => "Нулевик",
                2 => "Ип",
                3 => "Малая организация",
                4 => "Средняя организация"
            );
            ?>
            <div class="caption">Вид юр.лица</div>

            <select id="select2" class="area jq-selectbox" name="IP">
                <? foreach ($orgtype as $sys) { ?>
                    <option value="<?= $sys ?>">
                        <?= $sys; ?>
                    </option>
                <? } ?>
            </select>
            <input class="area req" type="text" name="NAME" placeholder="Ваше имя"> <br>
            <input class="area req" type="text" name="PHONE" id="phone" placeholder="Ваш телефон">
            <br>
            <input class="submit" type="submit" value="Заказать">
        </form>
    </div>
</div>


</div>

<script type="text/javascript">
    $(function () {
        $('.area').each(function (e) {
            var placeholder = $(this).attr('placeholder');
            $(this).wrap('<div class="inputwrapper"></div>').before('<span>' + placeholder + '</span>');
            $(this).on('focus', function () {
                var inputContent = $(this).val();
                if (inputContent == '') {
                    $(this).prev().addClass('visible');
                }

            });
            $(this).on('blur', function () {
                var inputContent = $(this).val();
                if (inputContent == '') {
                    $(this).prev().removeClass('visible');
                }
            });
        });
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lettering.js/0.7.0/jquery.lettering.min.js"></script>
<script type="text/javascript" src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>

<script type="text/javascript" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="/_plugins/jquery.paroller.min.js"></script>
<script type="text/javascript" src="/_js/scripts.min.js"></script>


<? $APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_RECURSIVE" => "N",
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/_include/metrics.php"
    )
); ?> <a href="javascript:void(0);" class="go_top js-go_top hidden_bt"></a>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter49961221 = new Ya.Metrika2({
                    id: 49961221,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/49961221" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->
</body>

</html>