<div id="key">
</div>
<h2 class="caption -one">Построение системы бухгалтерского учета под ключ</h2>
<p class="paragraph">
    Комплексное оказание услуги (от 120 тыс.рублей). Это путь от аудита компании до предоставления готового продукта в
    виде бухгалтера или бухгалтерского отдела (выстраивание бух учета в компании,т.е. целой системы)
</p>
<div class="block -one flex-justify-between ">
    <div class="scheme flex-justify-between">
        <h3 class="caption -two">Схема работы:</h3>
        <div class="item flex-justify-between">
            <div class="number">
                1
            </div>
            <div class="text">
                <h4 class="caption">Аудит</h4>
                <p>
                    Аудит текущей ситуации бухгалтерского учета
                </p>
                <span class="clock">1-2 дней</span>
            </div>
        </div>
        <div class="item flex-justify-between">
            <div class="number">
                2
            </div>
            <div class="text">
                <h4 class="caption">Подбор кандидатов</h4>
                <p>
                    Подбор кандидатов на роли. Тестирование, аттестация. Составление портрета бухгалтера или нескольких
                    бухгалтеров. 1 кандидат через неделю собеседование
                </p>
                <span class="clock">1 месяц</span>
            </div>
        </div>
        <div class="item flex-justify-between">
            <div class="number">
                3
            </div>
            <div class="text">
                <h4 class="caption">Бизнес-процессы</h4>
                <p>
                    Предположение модели бизнес-процессов, определение ролей, регламента работ и описание взаимодействия
                </p>
                <span class="clock">3-5 раб. дней</span>
            </div>
        </div>
        <div class="item flex-justify-between">
            <div class="number">
                4
            </div>
            <div class="text">
                <h4 class="caption">Внедрение</h4>
                <p>
                    Контроль выполнения. При необходимости обучение бухгалтера на предмет соответствия модели, что мы
                    предлагаем
                </p>
                <span class="clock">испытательный срок — 1-2 месяца</span>
            </div>
        </div>
    </div>
    <div class="description">
        <h3 class="caption -three">Для кого подойдёт:</h3>
        <ul>
            <li>Для холдингов</li>
            <li>Группы компаний</li>
            <li>Крупное производство</li>
            <li>Компании с оборотом от 50 млн. рублей</li>
        </ul>
        <h3 class="caption -four">Результат:</h3>
        <p>
            Выстроенная система бухгалтерского
            и налогового учета, описанные бизнес-процессы,
            полный контроль своей бухгалтерии
        </p>
    </div>
</div>
<div class="block -two">
    <h2 class="caption price">
        Цена: <br>
        от 120 000р.</h2>
    <button class="btn btn-outline-success -tel form1" type="submit">Заказать</button>

</div>
<section class="rope">
    <div class="fly">
        <div class="fly">
            <div class="fly">
                <div class="fly">
                    <div class="fly">
                        <div class="fly">
                            <div class="fly">
                                <div class="fly">
                                    <div class="fly">
                                        <div class="fly">
                                            <div class="fly">
                                                <div class="fly">
                                                    <div class="fly">
                                                        <div class="fly">
                                                            <div class="fly">
                                                                <div class="fly">
                                                                    <div class="fly">
                                                                        <div class="fly">
                                                                            <div class="fly">
                                                                                <div class="fly">
                                                                                    <div class="fly">
                                                                                        <div class="fly">
                                                                                            <div class="fly">
                                                                                                <div class="fly">
                                                                                                    <div class="fly">
                                                                                                        <div class="fly">
                                                                                                            <div class="fly">
                                                                                                                <div class="fly">
                                                                                                                    <div class="fly">
                                                                                                                        <div class="fly -last">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

