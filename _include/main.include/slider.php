<div class="background">
	<div class="container flex-justify-between">
		<div class="wrapper_left flex-column-around">
			<div class="advice">
 <img alt="clock" src="/_img/clock.png"> <span class="consultation">Бесплатная консультация <br>
				 9:00 до 18:00 по Мск</span>
			</div>
			<div class="years">
				 <span class="years__numeral">14</span> <br>
				 лет на рынке
			</div>
		</div>
		<div class="wrapper_right">
			<div class="slick-slider">
				<div class="inner_slide -one item active" id="get_0">
					<h1>Комплексное бухгалтерское <br>
					 сопровождение Вашего бизнеса в Москве</h1>
					<p class="paragraph">
						 Индивидуальный подход к каждому клиенту позволяет выстроить оптимальную систему бухгалтерского и налогового учета для любого бизнеса
					</p>
					<div class="services flex-justify-between">
						<div class="wrap">
							<div class="service -one">
								<div class="inner">
 <img alt="icon1" src="/_img/icon1.png">
									<p>
										 Выстроим систему бухгалтерского учета
									</p>
 <a class="services__link" href="#key">Посмотреть услугу</a>
								</div>
							</div>
						</div>
						<div class="wrap">
							<div class="service -two">
								<div class="inner">
 <img alt="icon2" src="/_img/icon2.png">
									<p>
										 Подберем квалифицированного бухгалтера
									</p>
 <a class="services__link" href="#accountant">Посмотреть услугу</a>
								</div>
							</div>
						</div>
						<div class="wrap">
							<div class="service -three">
								<div class="inner">
 <img alt="icon3" src="/_img/icon3.png">
									<p>
										 Возьмем Вашу бухгалтерию на аутсорсинг
									</p>
 <a class="services__link" href="#outsourcing">Посмотреть услугу</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="inner_slide -two item" id="get_1">
					<div class="box">
						<h1>Выстроим систему бухгалтерского учета</h1>
						<div class="wrap_slide flex-justify-between">
							<div class="block -one">
								<div class="paragraph">
 <img src="/_img/icon1.png" class="paragraph-img" alt="">
									<p class="paragraph">
										 Поможем начать с нуля или привести в порядок существующую систему учета, хранения и отчетности
									</p>
								</div>
								<ul class="sample">
									<h4 class="caption">Почему мы?</h4>
									<li>Разработанная методика</li>
									<li>Индивидуальный подход</li>
									<li>Выстроенные бизнес-процессы</li>
									<li>Контроль внедрения</li>
								</ul>
 <a class="services__link" href="">Посмотреть услугу</a>
							</div>
							<div class="block -two">
								<div class="circle -one">
									<div class="circle-text">
										 Выстроим систему бухгалтерского учета
									</div>
 <span class="circus-min">!</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="inner_slide -two item" id="get_2">
					<div class="box">
						<h1>Подберем квалифицированного бухгалтера за 1 месяц</h1>
						<div class="wrap_slide flex-justify-between">
							<div class="block -one">
								<div class="paragraph">
 <img src="/_img/icon2.png" class="paragraph-img" alt="">
									<p class="paragraph">
										 Подберем бухгалтера в соответствии с Вашими требованиями, проведем собеседования, проверим квалификацию, предоставим рекомендации
									</p>
								</div>
								<ul class="sample">
									<h4 class="caption">Что вы получите:</h4>
									<li>3 уровня отбора</li>
									<li>Сопровождение процесса внедрения кандидата</li>
									<li>Гарантия результата</li>
								</ul>
 <a class="services__link" href="">Посмотреть услугу</a>
							</div>
							<div class="block -two">
								<div class="circle -two">
									<div class="circle-text">
										 Подберем квалифицированного бухгалтера за 1 месяц<br>
									</div>
 <span class="circus-min">!</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="inner_slide -two item" id="get_3">
					<div class="box">
						<h1>Возьмем Вашу бухгалтерию на аутсорсинг</h1>
						<div class="wrap_slide flex-justify-between">
							<div class="block -one">
								<div class="paragraph">
 <img src="/_img/icon3.png" class="paragraph-img" alt="">
									<p class="paragraph">
										 Работайте спокойно - всю бухгалтерию мы возьмем на себя
									</p>
								</div>
								<ul class="sample">
									<h4 class="caption">Почему мы:</h4>
									<li>Комплексное сопровождение</li>
									<li>Сокращение расходов на содержание штатного бухгалтера</li>
									<li>Выгодные условия</li>
									<li>Гарантия качества</li>
								</ul>
 <a class="services__link" href="">Посмотреть услугу</a>
							</div>
							<div class="block -two">
								<div class="circle -three">
									<div class="circle-text">
										 Возьмем Вашу бухгалтерию на аутсорсинг
									</div>
 <span class="circus-min">!</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <br>