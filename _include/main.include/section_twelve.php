<div class="container">
    <div class="wrap">
        <div class="form">
            <h3 class="caption -gradient form__caption">Приведем бухгалтерию <br>
                в порядок</h3>
            <form class="flex-space-between js-form-submit" action="">
                <input type="hidden" value="FORM" name="EVENT"> <input type="hidden" value="REQUEST" name="GOAL"> <input
                        class="ms-form__input" type="hidden" value="Заказать звонок" name="THEME"> <input
                        class="area req" type="text" name="NAME" placeholder="Ваше имя"> <br>
                <input type="text" name="PHONE" class="area req" placeholder="Ваш телефон" x-autocompletet="tel"> <br>
                <input class="submit " type="submit" value="Заказать">
            </form>
        </div>
        <div class="applicants">
            <h2 class="caption -gradient">Соискателям</h2>
            <p class="paragraph">
                Мы не гарантируем Вам трудоустройства, но можем добавить резюме в свою базу. Если ваши компетенции
                потребуются на одной из вакансий, то мы свяжемся с Вами. Обязательно укажите в резюме диапазон ваших
                ожиданий по заработной плате и возраст
            </p>
            Отправляйте резюме на <a class="link" href="mailto:info@lavnikpartners.com">info@lavnikpartners.com</a>
        </div>
    </div>
    <div class="wrap__two">
        <div class="contacts" id="contacts">
            <h2 class="caption -gradient">Контакты</h2>
            <div class="address">
                Москва, м.Тушинская, Волоколамское шоссе, 89
            </div>
            <div class="metro">
                1.9км, Тушинская <br>
                ближайшее метро
            </div>
            <div class="tel">
                <a href="tel:+74953082290">8 (495) 308-22-90</a>
            </div>
            <div class="mail">
                <a href="mailto:info@lavnikpartners.com">info@lavnikpartners.com</a>
            </div>
            <div class="time">
                офис - с 9:00 до 18:00
            </div>
            <input class="submit form1" type="submit" value="Заказать звонок">
        </div>

        <div class="map">
            <div class="mapcontainer">
                <div class="map-wrap">
                    <div id="map">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<br>