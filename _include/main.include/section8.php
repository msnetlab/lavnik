<div class="container">
	<h2 class="caption -gradient -big">Найдем высококвалифицированного <br>
	 бухгалтера за 1 месяц</h2>
	<h2 class="caption -gradient -min">Найдем опытного бухгалтера за 1 месяц</h2>
	<div class="holder flex-justify-between">
		<div class="block -one">
			<p class="-paragraph">
				 Внушительный опыт нашей Компании в сфере бухгалтерского учета, налогов <br>
				 и финансов позволяет нам предоставлять работодателям результативных <br>
				 сотрудников, а так же гарантировать эффективность их работы
			</p>
			<div class="text">
				 Имеем собственную кадровую базу проверенных <br>
				 специалистов <span>Испытательный срок — 1-2 месяца</span>
			</div>
			<div class="form">
				<h3 class="form__caption">Нужен бухгалтер, <br>
				 мы поможем!</h3>
				<form class="flex-space-between js-form-submit form_success_2" action="">
 <input type="hidden" value="FORM" name="EVENT">
                    <input type="hidden" value="REQUEST" name="GOAL"/>
                    <input class="ms-form__input" type="hidden" value="Заказать звонок" name="THEME"> <br>
 <input class="area req" type="text" name="NAME" placeholder="Ваше имя"> <br>
 <input class="area req" type="text" name="PHONE" id="phone" placeholder="Ваш телефон"> <br>
 <input id="form1" class="submit" type="submit" value="Заказать">
				</form>
			</div>
		</div>
		<div class="block -two ">
			<h3 class="caption -gradient">Как мы это делаем?</h3>
			<div class="wrap flex-space-between">
				<div class="inner">
					 2 309 чел. <span class="pyramid">Поиск кандидатов</span>
				</div>
				<div class="inner">
					 803 чел. <span class="pyramid">Рассмотрение резюме</span>
				</div>
				<div class="inner">
					 120 чел. <span class="pyramid">Интервьюирование</span>
				</div>
				<div class="inner">
					 14 чел. <span class="pyramid">Предоставление кандидатов</span>
				</div>
				<div class="inner">
					 9 чел. <span class="pyramid">Подходящий кандидат</span>
				</div>
			</div>
			<p class="result -paragraph">
				 Мы нашли вам бухгалтера
			</p>
		</div>
	</div>
</div>
 <br>