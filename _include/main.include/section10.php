<div class="container">
    <h2 class="caption -gradient">Возьмем Вашу бухгалтерию на аутсорсинг</h2>
    <p class="paragraph">
        Работайте спокойно - всю бухгалтерию берем на себя. Без штрафов и посещения налоговой
    </p>
</div>
<div class="wrapper flex-justify-center">
    <div class="item service">
        <h3 class="service__caption">Нулевик</h3>
        <p>
            <span>Зарегистрировал ИП</span>, но не веду предпринимательскую деятельность. Однако мне <span>требуется подавать</span>
            декларацию в ФНС и внебюджетные фонды
        </p>
        <span class="time -two">Сдача отчетности</span>
        <div class="price -two">
            <span class="price__span">от</span> 1 500 руб. / <span class="price__span">месяц</span>
        </div>
        <a id="form1" class="btn form3 sel_1" href="#">Заказать</a>
    </div>
    <div class="item service">
        <h3 class="service__caption">ИП</h3>
        <p>
            Я индивидуальный предприниматель, у меня небольшой объем <span>первичной документации и есть сотрудники в штате.</span>
            Требуются услуги для малого бизнеса, а именно помощь в обработке "первички" и формирование налоговой и
            бухгалтерской отчетности
        </p>
        <span class="time -two">Налоговая и бухгалтерская отчетность, сотрудники - до 2-х человек</span>
        <div class="price -two">
            <span class="price__span">от</span> 8 000 руб. / <span class="price__span">месяц</span>
        </div>
        <a id="form1" class="btn form3 js-select" data-id="2" href="#">Заказать</a>
    </div>
    <div class="item service">
        <h3 class="service__caption">Малая организации</h3>
        <p>
            Мы - малое предприятие, в штате до трех сотрудников. Есть небольшой объем первичной документации. Нам <span>требуется бухгалтер, </span>
            который займется нашим кадровым, бухгалтерским и налоговым учетом
        </p>
        <span class="time -two">Налоговый и бухгалтерский учет, сотрудники - до 3-х человек</span>
        <div class="price -two">
            <span class="price__span">от</span> 11 500 руб. / <span class="price__span">месяц</span>
        </div>
        <a id="form1" class="btn form3 js-select" data-id="3" href="#">Заказать</a>
    </div>
    <div class="item service">
        <h3 class="service__caption">Средняя организация </h3>
        <p>
            Мы - средняя организация, в штате до семи человек. Есть постоянный объем первичной документации.Нам <span>требуется команда бухгалтеров,</span>
            которая будет вести наш кадровый, бухгалтерский и налоговый учет
        </p>
        <span class="time -two">Налоговый и бухгалтерский учет, сотрудники - до 7-х человек</span>
        <div class="price -two">
            <span class="price__span">от</span> 19 000 руб. / <span class="price__span">месяц</span>
        </div>
        <a class="btn form3 js-select" data-id="4" href="#">Заказать</a>
    </div>
</div>
<br>