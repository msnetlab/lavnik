<div class="container">
    <h2 class="caption">Наши услуги</h2>
    <div class="services-two flex-justify-between">
        <div class="service">
            <div class="inner -primary">
                <span class="time">за 2 месяца</span>
                <h3 class="service__caption">Выстроим систему бухгалтерского учета</h3>
                <p>
                    Поможем начать с нуля или привести в порядок существующую систему учета, хранения и отчётности
                </p>
                <ul>
                    <li>Разработанная методика</li>
                    <li>Индивидуальный подход</li>
                    <li>Выстроенные бизнес-процессы</li>
                    <li>Контроль внедрения</li>
                </ul>
                <div class="price">
                    от 120 000 руб.
                </div>
                <a class="btn form2 " href="#" value="1">Заказать</a>
                <div>
                    <a class="more_info" href="#key">Подробнее</a>
                </div>
            </div>
        </div>
        <div class="service">
            <div class="inner">
                <span class="time">за 1 месяц</span>
                <h3 class="service__caption">Подберем квалифицированного бухгалтера</h3>
                <p>
                    Найдём бухгалтера в соответствии с Вашими требованиями, проведем собеседования, проверим
                    квалификацию, представим рекомендации
                </p>
                <ul>
                    <li>3 уровня отбора</li>
                    <li>Сопровождение процесса внедрения кандидата</li>
                    <li>Гарантия результата</li>
                </ul>
                <div class="price">
                    1 оклад
                </div>
                <a class="btn form2  js-select" data-id="2" href="#" value="2">Заказать</a>
                <div>
                    <a class="more_info" href="#accountant">Подробнее</a>
                </div>
            </div>
        </div>
        <div class="service">
            <div class="inner">
                <span class="time">за 2 дня</span>
                <h3 class="service__caption">Возьмем Вашу бухгалтерию на аутсорсинг</h3>
                <p>
                    Работайте спокойно - всю бухгалтерию мы возьмем на себя
                </p>
                <ul>
                    <li>Комплексное сопровождение</li>
                    <li>Сокращение расходов на содержание штатного бухгалтера</li>
                    <li>Выгодные условия</li>
                    <li>Гарантия качества</li>
                </ul>
                <div class="price">
                    от 1 500 рублей
                </div>
                <a class="btn form3" href="#">Заказать</a>
                <div>
                    <a class="more_info" href="#outsourcing">Подробнее</a>
                </div>
            </div>
        </div>
    </div>
</div>
<br>