<div class="background">
    <div class="container flex-justify-between">
        <div class="wrapper_left flex-column-around">
            <div class="advice">
                <img alt="clock" src="/_img/clock.png"> <span class="consultation">Бесплатная консультация <br>
				 9:00 до 18:00 по Мск</span>
            </div>
            <div class="years">
                <span class="years__numeral">14</span> <br>
                лет на рынке
            </div>
        </div>
        <div class="wrapper_right">
            <div class="">
                <div class="inner_slide -two item" id="get_3">
                    <div class="box">
                        <h1>Возьмем Вашу бухгалтерию на аутсорсинг</h1>
                        <div class="wrap_slide flex-justify-between">
                            <div class="block -one">
                                <div class="paragraph">
                                    <img src="/_img/icon3.png" class="paragraph-img" alt="">
                                    <p class="paragraph">
                                        Работайте спокойно - всю бухгалтерию мы возьмем на себя
                                    </p>
                                </div>
                                <ul class="sample">
                                    <h4 class="caption">Почему мы:</h4>
                                    <li>Комплексное сопровождение</li>
                                    <li>Сокращение расходов на содержание штатного бухгалтера</li>
                                    <li>Выгодные условия</li>
                                    <li>Гарантия качества</li>
                                </ul>
                                <a class="services__link" href="">Посмотреть услугу</a>
                            </div>
                            <div class="block -two">
                                <div class="circle -three">
                                    <div class="circle-text">
                                        Возьмем Вашу бухгалтерию на аутсорсинг
                                    </div>
                                    <span class="circus-min">!</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>