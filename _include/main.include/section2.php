<div id="advantages"></div>
<div class="parallax -one my-paroller" data-paroller-factor="-0.2" data-paroller-type="foreground" data-paroller-direction="vertical">
 <img src="/_img/2.png" alt="">
</div>
<div  class="lavnik__two__background">
	<div class="container lavnik__two__wrap">
		<div class="caption">
			<h2>Преимущества работы с нами</h2>
			<p>
				 С нами Вы сможете вывести бухгалтерию своей Компании на качественно новый уровень по надежности и оперативности. Кроме того, мы даем возможность сократить расходы Вашей Компании по сравнению с затратами на штатных сотрудников Вашей бухгалтерии за счет исключений налоговых выплат по страховым взносам и НДФЛ. При этом сохраняется возможность отнесения 100% затрат на бухгалтерское сопровождение на расходы Вашей организации
			</p>
		</div>
		<div class="advantages">
			<div class="item">
				<div class="number">
					 1
				</div>
				<h3 class="item__caption">Наша команда - это опытные специалисты</h3>
				<p class="paragraph">
					 Финансисты, бухгалтеры и налоговые эксперты с многолетним опытом
				</p>
			</div>
			<div class="item">
				<div class="number">
					 2
				</div>
				<h3 class="item__caption">Являемся специалистами на рынке уже 14 лет</h3>
				<p class="paragraph">
					 Свою бухгалтерию нам доверили уже более 450 компаний в Москве и МО
				</p>
			</div>
			<div class="item">
				<div class="number">
					 3
				</div>
				<h3 class="item__caption">Собственная методика</h3>
				<p class="paragraph">
					 Мы выработали собственную методику построения бухгалтерского и налогового учета, а так же оценки и тестирования персонала финансово-экономического направления
				</p>
			</div>
			<div class="item">
				<div class="number">
					 4
				</div>
				<h3 class="item__caption">Нас рекомендуют</h3>
				<p class="paragraph">
					 56% наших клиентов приходят по рекомендациям своих друзей и знакомых
				</p>
			</div>
			<div class="item">
				<div class="number">
					 5
				</div>
				<h3 class="item__caption">Ответственность</h3>
				<p class="paragraph">
					 Мы дорожим своей репутацией, за 14 лет мы получили огромное количество благодарностей и положительных отзывов
				</p>
			</div>
			<div class="item">
				<div class="number">
					 6
				</div>
				<h3 class="item__caption">Имеем официальный статус аудиторской компании</h3>
				<p class="paragraph">
					 Есть CPO, сертификаты, лицензии международного стандарта
				</p>
			</div>
		</div>
	</div>
</div>
 <br>