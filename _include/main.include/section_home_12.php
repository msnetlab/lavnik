<div class="container">
    <div class="wrap">
        <div class="form">
            <h3 class="caption -gradient form__caption">Приведем бухгалтерию <br>
                в порядок</h3>
            <form class="flex-space-between js-form-submit" action="">
                <input type="hidden" value="FORM" name="EVENT"> <input type="hidden" value="REQUEST" name="GOAL"> <input
                        class="ms-form__input" type="hidden" value="Заказать звонок" name="THEME"> <input
                        class="area req" type="text" name="NAME" placeholder="Ваше имя"> <br>
                <input type="text" name="PHONE" class="area req" placeholder="Ваш телефон" x-autocompletet="tel"> <br>
                <input class="submit " type="submit" value="Заказать">
            </form>
        </div>
        <div class="applicants">
            <h2 class="caption -gradient">Соискателям</h2>
            <p class="paragraph">
                Мы не гарантируем Вам трудоустройства, но можем добавить резюме в свою базу. Если ваши компетенции
                потребуются на одной из вакансий, то мы свяжемся с Вами. Обязательно укажите в резюме диапазон ваших
                ожиданий по заработной плате и возраст
            </p>
            Отправляйте резюме на <a class="link" href="mailto:info@lavnikpartners.com">info@lavnikpartners.com</a>
        </div>
    </div>
    <div class="wrap__two">
        <div class="contacts" id="contacts">
            <h2 class="caption -gradient">Контакты</h2>
            <div class="address">
                Москва, м.Тушинская, Волоколамское шоссе, 89
            </div>
            <div class="metro">
                1.9км, Тушинская <br>
                ближайшее метро
            </div>
            <div class="tel">
                <a href="tel:+74953082290">8 (495) 308-22-90</a>
            </div>
            <div class="mail">
                <a href="mailto:info@lavnikpartners.com">info@lavnikpartners.com</a>
            </div>
            <div class="time">
                офис - с 9:00 до 18:00
            </div>
            <input class="submit form1" type="submit" value="Заказать звонок">
        </div>

        <div class="map">
            <div class="mapcontainer">
                <div class="map-wrap">
                    <div id="map">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://api-maps.yandex.ru/2.1/?lang=ru_RU">

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            ymaps.ready(function () {
                var myMap = new ymaps.Map('map', {
                        center: [55.831498, 37.410604],
                        zoom: 16,
                        controls: []

                    },

                    {suppressMapOpenBlock: true}),
                    // Создаём макет содержимого.
                    MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                        '<div class="map-block" style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
                    ),

                    myPlacemarkWithContent = new ymaps.Placemark([55.829158, 37.410604], {
                        hintContent: 'Лавник и партнеры',
                        balloonContent: '',
                        iconContent: ''
                    }, {
                        // Опции.
                        // Необходимо указать данный тип макета.
                        iconLayout: 'default#imageWithContent',
                        // Своё изображение иконки метки.
                        iconImageHref: '/_img/lavnik.png',
                        // Размеры метки.
                        iconImageSize: [170, 150],
                        // Смещение левого верхнего угла иконки относительно
                        // её "ножки" (точки привязки).
                        iconImageOffset: [-65, -135],
                        //Смещение слоя с содержимым относительно слоя с картинкой.
                        iconContentOffset: [15, 15],
                        // Макет содержимого.
                        //iconContentLayout: MyIconContentLayout
                    });
                myMap.geoObjects
                    .add(myPlacemarkWithContent);
            });
        });
    </script>
</div>
<br>