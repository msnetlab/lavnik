<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
?>

<div class="container">
    <div class="wrapper flex-justify-center">
        <div class="block -left flex-middle block_end">
            <h3 class="caption text-gradient">
                Чем мы ещё
                <br> можем быть
                <br> полезны
            </h3>
            <input class="submit form1" type="submit" value="Получить консультацию">
            <p>Наши лучшие специалисты
                проконсультируют вас по самым сложным
                вопросам</p>
        </div>
        <div class="block -right">
            <div class="flex-justify-between flex-wrap">
                <?  $i = 0;
                foreach ($arResult["ITEMS"] as $item) {
                    $i++;
                    if ($i > 6) {
                        break;
                    }
                    ?>
                <div class="item">
                    <div class="number"><?= $i?></div>
                    <h3 class="item__caption"><?= $item["~NAME"] ?></h3>
                    <p class="paragraph">
                        <?= $item["PROPERTIES"]["TEXT"]['VALUE']?>
                    </p>
                </div>
                <? } ?>
            </div>
            <div class="block__hidden">

                <div class="flex-justify-between flex-wrap ">
                    <?
                    $i = 0;
                    foreach ($arResult["ITEMS"] as $item) {
                        $i++;
                        if ($i <= 6) {
                            continue;
                        }
                        ?>
                        <div class="item">
                            <div class="number"><?= $i?></div>
                            <h3 class="item__caption"><?= $item["~NAME"] ?></h3>
                            <p class="paragraph">
                                <?= $item["PROPERTIES"]["TEXT"]['VALUE']?>
                            </p>
                        </div>

                    <? } ?>
                </div>
            </div>
            <?
            $i = 0;
            foreach ($arResult["ITEMS"] as $item) {
            $i++;
            if ($i <= 6) {
                continue;

            }
            ?>
                <span class="link js-show -one">Показать ещё
                                <span>+</span>
                            </span>
                <?if ($i>=7){
                    break;
                }

                    ?>
            <? }?>

            <span class="link js-show -two">Скрыть
                                <span>+</span>
                            </span>
        </div>
    </div>
</div>











