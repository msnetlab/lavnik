<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
?>
<div class="slider">
    <div class="slick-slider2">



        <? foreach ($arResult["ITEMS"] as $item) { ?>
        <div class="slide -one">
            <picture class="imagecontainer2">
                <?
                $imgPhoto = CFile::GetPath($item["PROPERTIES"]["PHOTO"]["VALUE"]);
                ?>
                <div class="block_img">

                    <div  class="item_img" style="background-image: url(<?= $imgPhoto ?>)" data-src="<?= $imgPhoto ?>"></div>

                </div>

            </picture>
            <div class="info">
                <h3 class="caption text-gradient">Лавник и Партнеры в лицах</h3>
                <div class="initials-mob">
                    <h4 class="initials text-gradient"><?= $item["~NAME"] ?></h4>
                    <div  class="initials_img"><div class="wrap-img" style="background-image: url(<?= $imgPhoto ?>)" data-src="<?= $imgPhoto ?>"></div></div>
                </div>
                <div class="position text-gradient"><?= $item["PROPERTIES"]["TITLE_SPECIAL"]['VALUE'] ?>
                </div>
                <div class="list-wrap">
                <span class="list">Отрасль:</span>
                <span class="list__value text-gradient"><?= $item["PROPERTIES"]["TITLE_IND"]['VALUE'] ?> </span>
                <br>
                <span class="list">Опыт работы:</span>
                <span class="list__value text-gradient"><?= $item["PROPERTIES"]["EXP"]['VALUE'] ?></span>
                </div>
                <p class="text-gradient -p" id="ajaxtest">&laquo;<?= $item["PROPERTIES"]["TEXT"]['VALUE']?>&raquo;
                   </p>

            </div>
        </div>



        <? } ?>
    </div>
    <!--<div class="info__read -on js-read-on">Показать +</div>
    <div class="info__read -off js-read-off">Скрыть +</div>-->
</div>


