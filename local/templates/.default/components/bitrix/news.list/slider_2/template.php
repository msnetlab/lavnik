<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
?>



        <div class="slider">
            <div class="slick-slider3">
                <? foreach ($arResult["ITEMS"] as $item) { ?>
                <div class="slide -one">
                    <h2 class="caption -gradient">Вы знаете что?</h2>
                    <div class="info">
                        <div class="percent text-gradient"><?= $item["PROPERTIES"]["PERCENT"]['VALUE'] ?></div>
                        <p>
                            <?= $item["PROPERTIES"]["TEXT"]['VALUE'] ?>
                        </p>


                        <a class="services__link" href="#outsourcing">Узнать как</a>
                        <div class="name">
                            <picture class="imagecontainer">
                                <?
                                $imgPhoto = CFile::GetPath($item["PROPERTIES"]["PHOTO"]["VALUE"]);
                                ?>
                                <div class="block_img">

                                    <div  class="item_img" style="background-image: url(<?= $imgPhoto ?>)" data-src="<?= $imgPhoto ?>"></div>

                                </div>
                            </picture>
                            <div class="name__inner">
                                <h4 class="name__caption"><?= $item["PROPERTIES"]["NAME"]['VALUE'] ?></h4>
                                <span><?= $item["PROPERTIES"]["TITLE_NAME"]['VALUE'] ?></span>
                            </div>
                        </div>
                    </div>
                </div>

                <? } ?>
            </div>
        </div>



