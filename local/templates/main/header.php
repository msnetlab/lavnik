<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
?>
<!DOCTYPE html>

    <head>
        <? $APPLICATION->ShowHead(); ?>
        <?$APPLICATION->SetTitle("Комплексное бухгалтерское сопровождение Вашего бизнеса в Москве");?>
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Комплексное бухгалтерское сопровождение Вашего бизнеса в Москве</title>
        <? CUtil::InitJSCore(array('jquery', 'window', 'popup', 'ajax', 'date')); ?>
        <meta property="og:image" content="<?= $APPLICATION->ShowProperty("og_image"); ?>"/>
        <meta property="og:image:url" content="<?= $APPLICATION->ShowProperty("og_image"); ?>"/>
        <meta property="og:title" content="<?= $APPLICATION->ShowProperty("og_title"); ?>"/>
        <meta property="og:description" content="<?= $APPLICATION->ShowProperty("og_description"); ?>"/>
        <meta property="og:url" content="<?= $APPLICATION->ShowProperty("og_url"); ?>"/>
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="<?= $APPLICATION->ShowProperty("og_url"); ?>" />
        <meta name="twitter:title" content="<?= $APPLICATION->ShowProperty("og_title"); ?>" />
        <meta name="twitter:description" content="<?= $APPLICATION->ShowProperty("og_description"); ?>" />
        <meta name="twitter:image:src" content="<?= $APPLICATION->ShowProperty("og_image"); ?>" />
        <link rel="image_src" href="<?= $APPLICATION->ShowProperty("og_image"); ?>" />
    <?
    $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => "/_include/for_all_pages.php",
            "AREA_FILE_SUFFIX" => "inc",
            "AREA_FILE_RECURSIVE" => "N",
            "EDIT_TEMPLATE" => ""
        )
    );
    ?>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="/_css/styles.css">
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script type="text/javascript" src="/_plugins/jquery.paroller.min.js"></script>
    <script type="text/javascript" src="/_js/scripts.min.js"></script>
    </head>
<body id="top">
<div id="panel">
    <? $APPLICATION->ShowPanel(); ?>
</div>
<?

?>

