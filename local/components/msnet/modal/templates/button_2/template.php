<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
//GetMessage("CONFIRM_PASSWORD");
?>
<a href="#msnet-modal-2" data-toggle="msnet-modal"
   class=" <?= $arParams["BUTTON_CLASSES"] ?>"
   data-title="<?= $arParams["FORM_TITLE"] ?>" 
   data-template="<?= $arParams["FORM_TEMPLATE"] ?>" 
   <?= implode(" ", $arParams["BUTTON_ATTRIBUTES"]) ?>
   ><?= $arParams["BUTTON_NAME"] ?></a>