<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
//GetMessage("CONFIRM_PASSWORD");
?>

<div id="msnet-modal-2" class="ms2-modal fade" tabindex="-1" role="dialog">
    <div class="ms2-modal-dialog" role="document">
        <div class="ms2-modal-content">
            <div class="ms2-modal-header">
                <a href="#" type="button" class="ms2-modal-close" data-dismiss="modal" aria-label="Close">
                </a>
            </div>
            <div class="ms2-modal-body js-msnet-modal-ajax">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->