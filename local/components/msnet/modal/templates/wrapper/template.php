<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
//GetMessage("CONFIRM_PASSWORD");
?>

<div id="msnet-modal" class="ms-modal fade" tabindex="-1" role="dialog">
    <div class="ms-modal-dialog" role="document">
        <div class="ms-modal-content">
            <div class="ms-modal-header">
                <a href="#" type="button" class="ms-modal-close" data-dismiss="modal" aria-label="Close">
                </a>
                <h4 class="ms-modal-title js-msnet-modal-title"></h4>
            </div>
            <div class="ms-modal-body js-msnet-modal-ajax">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->