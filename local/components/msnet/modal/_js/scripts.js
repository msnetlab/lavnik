{
    var MSModal = {
        s_button: "[data-toggle=msnet-modal]",
        s_form: false,
        s_form_title: ".js-msnet-modal-title",
        s_form_ajax: ".js-msnet-modal-ajax",
        buttons: false,
        data: {},
        init: function () {
            var _this = this;
            $("body").on("click", this.s_button, function (e) {
                e.preventDefault();
                _this.s_form = $(this).attr("href");
                _this.setTitle($(this).attr("data-title"));
                _this.data = $(this).data();
                _this.sendAjax("GET_CONTENT", _this.data, function (data) {
                    _this.setContent(data.html);
                    $(_this.s_form).modal();
                });
            });
            $(".ms-modal").on('show.bs.modal', function (e) {
                _this.fieldsShow();
                $('[name=PHONE]').mask("+7(999)-999-99-99");
            });
            $(".ms-modal").on('hide.bs.modal', function (e) {
//                _this.fieldsHide(); 
            });
        },
        setTitle: function (title) {
            $(this.s_form).find(this.s_form_title).html(title);
        },
        setContent: function (html) {
            $(this.s_form).find(this.s_form_ajax).html(html);
        },
        fieldsShow: function () {
            var _this = this;
            console.log(1, this.s_form);
            var _item = $(this.s_form).find(".ms-animate").not(".shown").first();
            if (_item.length > 0) {
                _item.addClass("shown");
                setTimeout(function () {
                    _this.fieldsShow();
                }, 200);
            }
        },
        fieldsHide: function () {
            var _this = this;
            var _item = $(this.s_form).find(".ms-animate.shown").first();
            if (_item.length > 0) {
                _item.removeClass("shown");
                setTimeout(function () {
                    _this.fieldsHide();
                }, 200);
            }
        },
        sendAjax: function (_action, _data, _callBack) {
            _callBack = _callBack || function () {
            };
            $.ajax({
                url: '/local/components/msnet/modal/ajax.php',
                dataType: 'json',
                type: 'POST',
                data: {
                    'action': _action,
                    'data': _data
                },
                error: function (data) {
                    console.log(data);
                },
            }).done(function (data) {
                _callBack(data);
            });
        },
    }
}
$(function () {
    MSModal.init();
});