<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
$action = $_REQUEST['action'];
$data = $_REQUEST['data'];
switch ($action) {
    case "GET_CONTENT":
        $out = $APPLICATION->IncludeComponent(
                "msnet:modal", $data["template"], array(
            "DATA" => $data,
            "COMPOSITE_FRAME_MODE" => "A",
            "COMPOSITE_FRAME_TYPE" => "AUTO",
            "FORM_TEMPLATE" => "",
            "URL_TEMPLATES" => "",
            "COMPONENT_TEMPLATE" => "",
            "BUTTON_NAME" => "",
            "BUTTON_CLASSES" => "",
            "BUTTON_ATTRIBUTES" => array(),
            "RETURN_RESULT" => true,
                ), false
        );
        echo json_encode($out);
        break;
}

