<?
\Bitrix\Main\Loader::includeModule('msnet.site');
define("CACHE_TYPE", "N");
define("SIZE_LIMIT", 1024 * 500); //500 кб


CModule::AddAutoloadClasses(
    '', // не указываем имя модуля
    array(
        // ключ - имя класса, значение - путь относительно корня сайта к файлу с классом
//    'Mobile_Detect' => '/local/php_interface/classes/mobile_detect.php',
    )
);


//$_GLOBALS["SYSTEM_LIST"] = array(
//    1 => "Выстроим систему бухгалтерского учета",
//    2 => "Найдем квалифицированного бухгалтера за 1 месяц",
//);
//$_GLOBALS["IP"] = array(
//    1 => "Нулевик",
//    2 => "Ип",
//    3 => "Малая организация",
//    4 => "Средняя организация"
//);

include_once("includes/functions.php");
include_once("includes/handlers.php");
include_once("includes/const.php");


getSettings();
