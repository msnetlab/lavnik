<?php

//Отправка смс для событий начинающихся с SMS_
AddEventHandler("main", "OnBeforeEventAdd", "OnBeforeEventAddHandler");
AddEventHandler("main", "OnBeforeUserUpdate", "OnBeforeUserUpdateHandler");

AddEventHandler("sale", "OnBeforeBasketAdd", "MyOnBeforeBasketAdd");

AddEventHandler("sale", "OnOrderSave", "OnOrderSaveHandler");

function MyOnBeforeBasketAdd(&$arFields) {
    //echo $arFields;
//    $arFields["PROPS"]["RND"] = array(
//        "CODE" => "RND",
//        "VALUE" => date("dmYHis"),
//        "NAME" => "RAND"
//    );
}

function OnBeforeEventAddHandler(&$event, &$lid, &$arFields, &$message_id) {

    if (mb_substr($event, 0, 4) == 'SMS_') {

        $arFilter = array(
            "ACTIVE" => "Y",
            "TYPE_ID" => $event,
            "SITE_ID" => $lid
        );

        if ($message_id) {
            $arFilter['ID'] = $message_id;
        }

        $rsMess = CEventMessage::GetList($by = "site_id", $order = "desc", $arFilter);
        while ($template = $rsMess->Fetch()) {
            $mess = $template['MESSAGE'];
            $to = $template['EMAIL_TO'];
            if (!$arFields["SITE_NAME"]) {
                $rsSites = CSite::GetByID("s1");
                $arSite = $rsSites->Fetch();
                $arFields["SITE_NAME"] = $arSite["SITE_NAME"];
            }
            foreach ($arFields as $keyField => $arField) {
                $mess = str_replace('#' . $keyField . '#', $arField, $mess); //подставляем значения в шаблон
                $to = str_replace('#' . $keyField . '#', $arField, $to);
            }
            if (CModule::IncludeModule("smsc.sms")) {
                $sms = new SMSC_Send;
                $phone = preg_replace('/\D/', '', $to);
                $sms->Send_SMS($phone, $mess);
            }
        }
    }
}

function OnBeforeUserUpdateHandler(&$arFields) {
    $isUserUpdate = $_REQUEST["USER_UPDATE"];
    $OLD_PASSWORD = $_REQUEST["OLD_PASSWORD"];
    if ($isUserUpdate == "Y") {
        $newPassword = !empty($arFields["PASSWORD"]);
        $newPasswordConfirm = !empty($arFields["CONFIRM_PASSWORD"]);
        $passwordsAreSame = $arFields["PASSWORD"] === $arFields["CONFIRM_PASSWORD"];
        $newPasswordsIsOK = $newPassword && $newPasswordConfirm && $passwordsAreSame;
//        if (!$passwordsAreSame) {
//            return false;
//        }
        if ($newPasswordsIsOK) {
            if (!empty($OLD_PASSWORD)) {
                if (!isUserPassword($arFields["ID"], $OLD_PASSWORD)) {
                    return false;
                }
            }
        }
    }
}

function OnOrderSaveHandler($ID, $arFields, $arOrder, $isNew) {
    global $USER;

    CModule::IncludeModule("main");

    $rsUsers = CUser::GetList(($by = "ID"), ($order = "desc"), array("ID" => $USER->GetID())); // выбираем пользователей
    if ($arUser = $rsUsers->GetNext()) {
        if(empty($arUser['EMAIL'])) {
            $cuser = new CUser();
            $fields = Array(
                "EMAIL" => $arOrder['ORDER_PROP'][10],
            );
            $cuser->Update($arUser["ID"], $fields);
        }
    }
}